/**
 * @author Jônathas Assunção 
 * 
 * Execução do tutorial: Tutorial Websockets com Socket.io
 */

var sleep = require("system-sleep"); 

const app = require("express")();
const http = require("http").createServer(app);
const io = require("socket.io")(http);

app.get("/", function(req, res) {
  res.sendFile("index.html", { root: "./" });
});

io.on("connection", socket => {
  socket.on("conectar", () => {
    socket.emit("msg", "Bem vindo ao mundo dos websockets");
  });
  socket.on("desconectar", () => {
    socket.emit("msg", "Você foi desconectado");
  });

  socket.on("msg", msg => {
    sleep(5000);
    try {
      socket.emit("calc", eval(msg));
    } catch (error) {
      socket.emit("calc", "Expressão inválida!");
    }
  });
});

http.listen(3000, function() {
  console.log("listening on port 3000");
});

// http.close(); // Fechando a conexão para o próximo exemplo
