
# Executando Tutorial [Websockets utilizando Socket.io](https://gitlab.com/tetrinetjs/tutorials/blob/master/Websockets%20utilizando%20Socket%20io/Websockets%20utilizando%20Socket%20io.ipynb)

[![node](https://img.shields.io/badge/node-8.10.0-green.svg)](<[https://nodejs.org/en/download/](https://nodejs.org/en/download/)>)
[![npm](https://img.shields.io/badge/npm-6.9.0-red.svg)](https://www.npmjs.com/)


### Para baixar dependências:

    npm install

### Para executar:

    node index.js

URL: [http://localhost:3000/](http://localhost:3000/)

### Aluno: Jônathas Assunção Alves - 201613279
